# ACreator-ng

Der ACreator-ng soll ein Tool zur Erschaffung eines Charakters im Rollenspiel System Arcane Codex von Nackter Stahl werden. Das "ng" im Namen steht dabei für das Framework "Angular2", mit welchem diese Applikation geschrieben wird.
Dazu gehört die Auswahl der Rasse, die Attributswerte, Fähigkeiten und deren Ränge, Vorzüge und Schwächen, Vergabe der Zusatzpunkte, Kauf der Ausrüstung und abschließende Übersicht über die Werte.  
Dabei soll auch validiert werden, ob der Charakter alle Bedingungen der Charaktererstellung (Nicht zu wenig, nicht zuviele Fertigkeits- und andere Punkte vergeben, etc.)

Da die Tabellen aus dem Regelwerk nicht verbreitet werden dürfen, müssen diese in der finalen Version von Hand eingegeben werden. Für Backups, und damit man z.B. auch seine Helden auf einem PC anfangen und dann auf einem anderen Gerät (z.B. Smartphone) weiterbearbeiten kann, soll ein Import-Export-Mechanismus eingefügt werden.

Der aktuelle Build des Master Branches kann auf https://acreator.kalyndor.de verwendet werden.

## Loslegen

### Voraussetzungen

Installationsanweisungen sind auf den jeweiligen Seiten angegeben

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/en/) Version 8
- [Angular CLI](https://github.com/angular/angular-cli) Version 6
- [Python](https://www.python.org/) Version 2.7 oder höher (nur für Deployment)

### Installing

Dieses Repository klonen

```
git clone git@gitlab.com:CGundlach/ACreator-ng.git
```

Ein Terminal öffnen (z.B. Bash, cmd, PowerShell) und die Dependencies via npm installieren

```
npm install
```

Fertig! Der Entwicklungsserver kann jetzt gestartet werden

### Entwicklungsserver

Führe den Befehl `ng serve` aus, um einen Entwicklungsserver zu starten.
Navigiere zu `http://localhost:4200/`. Die App wird automatisch neuladen, wann immer Quelldateien geändert werden.

### Codegerüste

Führe `ng generate component component-name` aus, um eine neue Komponente zu erstellen.  
Variationen: `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Führe `ng build` aus, um das Projekt zu kompilieren. Die Artefakte werden dabei im `dist/`-Ordner gespeichert. Mit dem Parameter `--prod` kann ein Production-Build erstellt werden.

### Unit Tests

Tests sind zur Zeit nicht implementiert. Sobald sie implementiert sind, können sie mit `ng test` mit [Karma](https://karma-runner.github.io) ausgeführt werden.

### End-to-end Tests

E2E-Tests sind zur Zeit nicht implementiert. Sobald sie implementiert sind, können sie mit `ng e2e` via [Protractor](http://www.protractortest.org/) ausgeführt werden.

### Static Code Analysis

Dieses Projekt verwendet [tslint](https://palantir.github.io/tslint/) und [prettier](https://prettier.io/). Konfigurationsdateien sind im Repository enthalten. Korrekte Formatierung kann mit `ng lint` geprüft werden.

### Deploy

Führe `python ./bin/deploy.py -h <host>:<port> -u <FTPuser> -p <password>` aus, um das kompilierte Projekt im Root-Verzeichnes eines FTP-Benutzers auf deinem Server zu deployen.

## Verwendete Software

- [Angular v5](https://angular.io/) - Framework
- [npm](https://www.npmjs.com/) - Dependency Management
- [Angular Material v5](https://material.angular.io) - Theme

## Beitragen

Zur Zeit nehme ich keinen Code von Anderen an, es können aber gerne Fehler berichtet werden. Feature Requests werden angenommen, sobald die Grundfunktionen alle implementiert sind.

## Authors

- Christopher Gundlach - @CGundlach

## Lizenz

Copyright (c) 2018 Christopher Gundlach

Zur Zeit wird keine Lizenz vergeben

## Disclaimer

Arcane Codex und Nackter Stahl sind ein eingetragenes Warenzeichen von Nackter Stahl®, Köln. Copyright by Nackter Stahl® 1999-2018.
