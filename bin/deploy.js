//doesn't work. Rework callback order

return;

var fs = require('fs');
var Client = require('ftp');
var path = require('path');

const c = new Client();
const source = 'dist';

let fileList = [];

if (!process.argv[2]) {
  console.log("WARN: host information not set - defaulting to 'localhost'");
}
if (!process.argv[3]) {
  console.log("WARN: port information not set - defaulting to port '21'");
}
if (!process.argv[4]) {
  console.log("WARN: username not set - defaulting to 'anonymous'");
}
if (!process.argv[5]) {
  console.log("WARN: password not set - defaulting to 'anonymous@'");
}

const ftpconfig = {
  host: process.argv[2],
  port: process.argv[3],
  user: process.argv[4],
  password: process.argv[5],
};

process.chdir(source);

c.connect(ftpconfig);

c.on('ready', () => {
  c.list('', (error, items) => {
    items.forEach((item, index, array) => {
      if (item.type === 'd') {
        c.rmdir(item.name, true, err => {
          console.log('removing directory: ' + item.name);
          if (err) {
            throw err;
          }
        });
      } else if (item.type === '-') {
        c.delete(item.name, err => {
          console.log('deleting file: ' + item.name);
          if (err) {
            throw err;
          }
        });
      }
    });
    processDirectory('./');
    fileList.forEach((file, i, array) => {
      uploadFile(file);
    });
    c.end();
  });
});

c.on('close', () => {
  console.log('connection closed');
});
c.on('end', () => {
  console.log('connection ended');
});

function processDirectory(directory) {
  c.mkdir(directory.replace(/\\/g, '/'), true, error => {
    console.log('creating directory: ' + directory);
    if (error) {
      throw error;
    }
  });
  const items = fs.readdirSync(directory);
  items.forEach((item, i, array) => {
    item = path.join(directory, item);
    const stats = fs.statSync(item);
    if (stats.isFile()) {
      fileList.push(item);
    }
    if (stats.isDirectory()) {
      processDirectory(item);
    }
  });
}

function uploadFile(file) {
  const dest = file.replace(/\\/g, '/');
  c.put(file, dest, err => {
    console.log('uploading file: ' + file);
    if (err) {
      throw err;
    }
  });
}
