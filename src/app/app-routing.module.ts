import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroComponent } from './components/hero/hero.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/hero' },
  { path: 'hero', component: HeroComponent },
  { path: 'hero/:heroname', component: HeroComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
