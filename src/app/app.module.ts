import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule,
  MatTableModule,
  MatTabsModule,
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

import { AttributesService } from './services/attributes.service';
import { HeroBackupService } from './services/hero-backup.service';
import { HeroService } from './services/hero.service';
import { RaceService } from './services/race.service';
import { SkillsService } from './services/skills.service';

import { AppComponent } from './app.component';
import { AttributeComponent } from './components/hero/attributes/attribute/attribute.component';
import { AttributesComponent } from './components/hero/attributes/attributes.component';
import { EquipComponent } from './components/hero/equip/equip.component';
import { ExtrapointsComponent } from './components/hero/extrapoints/extrapoints.component';
import { HeroComponent } from './components/hero/hero.component';
import { QualitiesComponent } from './components/hero/qualities/qualities.component';
import { RacedetailsComponent } from './components/hero/race/details/racedetails.component';
import { RaceComponent } from './components/hero/race/race.component';
import { SkillsComponent } from './components/hero/skills/skills.component';
import { StatsComponent } from './components/hero/stats/stats.component';
import { NotesComponent } from './components/hero/skills/notes/notes.component';

@NgModule({
  declarations: [
    AppComponent,
    RaceComponent,
    StatsComponent,
    SkillsComponent,
    EquipComponent,
    ExtrapointsComponent,
    HeroComponent,
    QualitiesComponent,
    RacedetailsComponent,
    AttributesComponent,
    AttributeComponent,
    NotesComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatSelectModule,
    MatListModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
  ],
  providers: [RaceService, HeroService, AttributesService, HeroBackupService],
  bootstrap: [AppComponent],
})
export class AppModule {}
