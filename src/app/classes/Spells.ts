// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { IACObject } from './iacobject';

export interface ISpell extends IACObject {
  type: string;
  school?: string;
  stat: string;
  grade: number;
  resist?: number;
  castTime: string;
  range: string;
  duration: string;
}
