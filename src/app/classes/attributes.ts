// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { IACObject } from './iacobject';
export interface IAttribute extends IACObject {
  short: string;
}

export class Attribute implements IAttribute {
  book: string;
  page: number;
  notes?: string;
  name: string;
  short: string;
  // bonus: number;
  value: number;

  constructor(attributeProto: IAttribute, value?: number, notes?: string) {
    this.book = attributeProto.book;
    this.page = attributeProto.page;
    this.name = attributeProto.name;
    this.short = attributeProto.short;
    if (value) {
      this.value = value;
    } else {
      this.value = 3;
    }
    if (notes) {
      this.notes = notes;
    }
  }

  public get bonus(): number {
    return this.value - 5;
  }
}
