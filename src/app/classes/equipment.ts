// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { IACObject } from './iacobject';

export interface IEquipment extends IACObject {
  cost?: number;
  quality?: string;
  category: string;
  weight?: number;
  count?: number;
}

export class Package {
  content: IEquipment[];
}

export interface IArmor extends IEquipment {
  armorValue: number;
  area?: string;
  minStrength: number;
  special?: string;
}

export interface IWeapon extends IEquipment {
  skill: string;
  damage: IDamage;
  minStrength: number;
  size: string;
  special?: string;
}

export interface IRangedWeapon extends IWeapon {
  reloadTime: number;
  ammunition?: number;
  range: IRange;
}

export interface ICannon extends IRangedWeapon {
  team: number;
}

export interface IShield extends IWeapon, IArmor {
  paradeBonus: number;
}

interface IRange {
  short: number;
  medium: number;
  far: number;
}

interface IDamage {
  rawDamage: number;
  diceDamage: number;
}
