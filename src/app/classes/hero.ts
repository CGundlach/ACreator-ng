// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Attribute } from './attributes';
import { IEquipment } from './equipment';
import { IOrigin } from './origin';
import { IQuality } from './qualities';
import { IRace } from './race';
import { ISchool } from './schools';
import { ISkill } from './skills';

export class Hero {
  name: string;
  origin: IOrigin;
  race: IRace;
  attributes: Attribute[];
  skills: ISkill[];
  qualities: IQuality[];
  schools: ISchool[];
  equipment: IEquipment[];
  money: number;

  constructor(name?: string, hero?: Hero) {
    this.name = name;
    if (hero) {
      if (hero.race) {
        this.race = hero.race;
      }
      if (hero.attributes) {
        for (const attribute of hero.attributes) {
          hero.attributes.splice(
            hero.attributes.findIndex(item => item.name === attribute.name),
            1,
            new Attribute(
              {
                name: attribute.name,
                book: attribute.book,
                page: attribute.page,
                short: attribute.short,
              },
              attribute.value,
              attribute.notes
            )
          );
        }
        this.attributes = hero.attributes;
      }
      if (hero.skills) {
        this.skills = hero.skills;
      }
    }
  }
}
