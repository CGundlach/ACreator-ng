// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

export interface IACObject {
  name: string;
  book: string;
  page: number;
  notes?: string;
}
