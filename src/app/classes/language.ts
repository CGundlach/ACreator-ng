// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { IACObject } from './iacobject';

export class Language implements IACObject {
  name: string;
  book: string;
  page: number;
  grade?: number;
}
