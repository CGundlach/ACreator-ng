// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { IACObject } from './iacobject';
import { Language } from './language';

export interface IOrigin extends IACObject {
  religion: string;
  language: Language;
}
