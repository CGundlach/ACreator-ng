// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { IACObject } from './iacobject';

export interface IQuality extends IACObject {
  cost: number;
  minCost?: number;
  maxCost?: number;
  effect?: string;
}
