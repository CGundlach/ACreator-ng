// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { IACObject } from './iacobject';
import { IOrigin } from './origin';

export interface IRace extends IACObject {
  origins?: IOrigin[];
  size: string;
  moveSpeed: IMoveSpeed;
  moveSpeedAir?: IMoveSpeed;
  skillBoni?: ISkillBonus[];
  special?: string[];
  attributes: IRaceAttr[];
}

interface IMoveSpeed {
  combat: number;
  march?: number; // wenn nicht angegeben: combat * 2
  sprint?: number; // wenn nicht angegeben: combat * 8
  swim?: number; // wenn nicht angegeben: combat
}

interface ISkillBonus {
  name: string;
  bonus: number;
}

interface IRaceAttr extends IACObject {
  min: number;
  max: number;
  average: number;
}
