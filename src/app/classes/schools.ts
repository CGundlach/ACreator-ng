// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { IACObject } from './iacobject';
import { ISpell } from './Spells';

export interface ISchool extends IACObject {
  rank: number;
  special?: string;
}

export interface IMagicSchool extends ISchool {
  type: string;
  knownSpells?: ISpell[];
}
