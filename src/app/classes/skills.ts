// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { IACObject } from './iacobject';

export interface ISkill extends IACObject {
  attribute?: string;
  rank?: number;
}
