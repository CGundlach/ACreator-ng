// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { AttributesService } from './../../../../services/attributes.service';

import { Attribute } from '../../../../classes/attributes';
import { RaceService } from '../../../../services/race.service';

@Component({
  selector: 'app-attribute',
  templateUrl: './attribute.component.html',
  styleUrls: ['./attribute.component.css'],
})
export class AttributeComponent implements OnInit, OnDestroy {
  @Input() attribute: Attribute;

  public raceConstraints = { min: 3, max: 10 };

  private raceSub: Subscription;

  constructor(
    private attributesService: AttributesService,
    private raceService: RaceService
  ) {}

  ngOnInit() {
    this.raceSub = this.raceService.getCurRace$().subscribe(race => {
      if (race) {
        const raceAttribute = race.attributes.find(
          attribute => attribute.name === this.attribute.name
        );
        this.raceConstraints = {
          min: raceAttribute.min,
          max: raceAttribute.max,
        };
      }
    });
  }

  ngOnDestroy() {
    this.raceSub.unsubscribe();
  }

  decrementAttribute() {
    this.attributesService.decrementAttributeValue(this.attribute.short);
  }
  incrementAttribute() {
    this.attributesService.incrementAttributeValue(this.attribute.short);
  }

  setAttributeValue(value: number) {
    this.attributesService.setAttributeValue(this.attribute.short, value);
  }
}
