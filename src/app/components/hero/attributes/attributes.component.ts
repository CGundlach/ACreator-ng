// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Component, Input, OnInit } from '@angular/core';

import { AttributesService } from './../../../services/attributes.service';

@Component({
  selector: 'app-attributes',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.css'],
})
export class AttributesComponent implements OnInit {
  @Input() heroName;

  attributes$ = this.attributesService.getCurAttributes$();

  constructor(private attributesService: AttributesService) {}

  ngOnInit() {}
}
