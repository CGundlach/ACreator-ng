import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-extrapoints',
  templateUrl: './extrapoints.component.html',
  styleUrls: ['./extrapoints.component.css'],
})
export class ExtrapointsComponent implements OnInit {
  @Input() heroName;
  constructor() {}

  ngOnInit() {}
}
