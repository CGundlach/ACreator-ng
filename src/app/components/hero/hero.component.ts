// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { HeroService } from './../../services/hero.service';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css'],
})
export class HeroComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private heroService: HeroService
  ) {}

  heroList$ = this.heroService.getHeroList$();
  curHero$ = this.heroService.getCurrentHero$();
  curHeroName: string;
  editMode = false;
  nameForm = new FormGroup({
    newName: new FormControl(),
  });

  hasRace = false;

  ngOnInit() {
    this.curHero$.subscribe(res => {
      this.hasRace = false;
      if (res) {
        this.curHeroName = res.name;
        this.router.navigate(['hero', this.curHeroName]);
        if (typeof res.race !== 'undefined') {
          this.hasRace = true;
        }
      }
    });
    this.route.params.subscribe(params => {
      if (params.heroname) {
        if (!this.heroService.heroExists(params.heroname)) {
          this.router.navigate(['hero']);
        }
        this.setHero(params.heroname);
      }
    });
  }

  public createNewHero() {
    this.heroService.createNewHero();
  }

  public setHero(heroName: string) {
    this.heroService.setCurrentHero(heroName);
  }

  public toggleEditMode() {
    if (this.editMode) {
      this.heroService.setHeroName(
        this.curHeroName,
        this.nameForm.controls.newName.value
      );
    }
    this.editMode ? (this.editMode = false) : (this.editMode = true);
  }

  public removeHero(heroName: string) {
    this.heroService.removeHero(heroName);
    this.heroService.setCurrentHero();
    this.router.navigate(['hero']);
  }
}
