import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-qualities',
  templateUrl: './qualities.component.html',
  styleUrls: ['./qualities.component.css'],
})
export class QualitiesComponent implements OnInit {
  @Input() heroName;
  constructor() {}

  ngOnInit() {}
}
