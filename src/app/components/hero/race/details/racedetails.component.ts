// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Component, Input } from '@angular/core';
import { IRace } from '../../../../classes/race';

@Component({
  selector: 'app-racedetails',
  templateUrl: './racedetails.component.html',
  styleUrls: ['./racedetails.component.css'],
})
export class RacedetailsComponent {
  @Input() race: IRace;
  constructor() {}
}
