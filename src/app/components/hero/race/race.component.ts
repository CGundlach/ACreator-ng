// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Component, OnInit } from '@angular/core';
import { RaceService } from './../../../services/race.service';

@Component({
  selector: 'app-race',
  templateUrl: './race.component.html',
  styleUrls: ['./race.component.css'],
})
export class RaceComponent implements OnInit {
  selectedRace$ = this.raceService.getCurRace$();
  selectedRaceName: string;
  raceList$ = this.raceService.getRaceList$();

  constructor(private raceService: RaceService) {}

  ngOnInit() {
    this.selectedRace$.subscribe(res => {
      if (res) {
        this.selectedRaceName = res.name;
      } else {
        this.selectedRaceName = '';
      }
    });
  }

  setRace(raceName: string) {
    this.raceService.setCurRace(raceName);
  }
}
