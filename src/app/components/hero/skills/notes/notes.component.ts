// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Component, Input, OnInit } from '@angular/core';
import { SkillsService } from '../../../../services/skills.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css'],
})
export class NotesComponent implements OnInit {
  @Input() name: string;
  @Input() notes: string;

  editMode = false;

  constructor(private skillServ: SkillsService) {}

  ngOnInit() {}

  public setNotes() {
    this.skillServ.setNotes(this.name, this.notes);
    this.editMode = false;
  }

  public editNotes() {
    this.editMode = true;
  }
}
