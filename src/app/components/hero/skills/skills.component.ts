// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ISkill } from '../../../classes/skills';
import { AttributesService } from '../../../services/attributes.service';
import { SkillsService } from '../../../services/skills.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css'],
})
export class SkillsComponent implements OnInit {
  @Input() heroName;

  @ViewChild('skillsTable') skillsTable: MatTable<any>;

  skills = new MatTableDataSource([]);

  skillList$ = this.skillServ.getSkillList$();
  skillList: ISkill[] = [];

  selectedSkill: string;

  editmode: string[] = [''];

  columnsToDisplay = [
    'skillRemove',
    'skillName',
    'skillRank',
    'skillAttribute',
    'skillValue',
    'skillNotes',
  ];

  constructor(
    private attrServ: AttributesService,
    private skillServ: SkillsService
  ) {}

  attributeBonus = new Array<{ name: string; bonus: number }>();

  ngOnInit() {
    this.attrServ.getCurAttributes$().subscribe(attributes => {
      if (attributes) {
        for (const attribute of attributes) {
          let index = this.attributeBonus.findIndex(
            item => item.name === attribute.name
          );
          if (index === -1) {
            this.attributeBonus.push({
              name: attribute.name,
              bonus: attribute.bonus,
            });
            index = this.attributeBonus.findIndex(
              item => item.name === attribute.name
            );
          }
          this.attributeBonus[index].bonus = attribute.bonus;
        }
      }
    });

    this.skillServ.getCurSkills$().subscribe(res => {
      if (res) {
        this.skills.data = res;
        this.pruneSkillList();
      }
    });
    this.skillList$
      .pipe(
        take(2),
        map(value => this.pruneSkillList())
      )
      .subscribe();
  }

  getSkillValue(rank: number, attributeName: string): number {
    return +rank + +this.getAttributeBonus(attributeName);
  }

  getAttributeBonus(name: string): number {
    const bonus = this.attributeBonus.find(item => item.name === name).bonus;
    return typeof bonus !== 'undefined' ? bonus : 0;
  }

  public addSkill() {
    this.skillServ.addSkill(this.selectedSkill);
  }

  public removeSkill(name: string) {
    this.skillServ.removeSkill(name);
  }

  public incSkill(name: string) {
    this.skillServ.incSkill(name);
  }

  public decSkill(name: string) {
    this.skillServ.decSkill(name);
  }

  public setNotes(name: string, notes: string) {
    this.skillServ.setNotes(name, notes);
  }

  public skillExists(name: string): boolean {
    const index = this.skills.data.findIndex(item => item.name === name);
    return index === -1 ? false : true;
  }

  public reorderSkill(name: string, newPosition: number) {
    this.skillServ.reorder(name, newPosition);
  }

  private pruneSkillList() {
    this.skillList$
      .pipe(
        take(1),
        map(skillList => {
          this.skillList = [];
          for (const skill of skillList) {
            if (!this.skillExists(skill.name)) {
              this.skillList.push(skill);
            }
          }
        })
      )
      .subscribe();
  }
}
