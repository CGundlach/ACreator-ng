// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { HeroService } from './hero.service';
import { RaceService } from './race.service';

import { Attribute, IAttribute } from './../classes/attributes';

@Injectable()
export class AttributesService {
  private curAttributes = new Array<Attribute>();
  private attributesJSON = './../../assets/data/attributes.json';
  private curAttributes$ = new BehaviorSubject<Attribute[]>(undefined);

  private curHero$ = this.heroService.getCurrentHero$();

  private raceConstraints = new Array<{
    name: string;
    min: number;
    max: number;
  }>();

  constructor(
    private http: HttpClient,
    private heroService: HeroService,
    private raceService: RaceService
  ) {
    this.curHero$.subscribe(hero => {
      if (hero) {
        if (typeof hero.attributes === 'undefined') {
          this.loadAttributes().subscribe(response => {
            const attributes = new Array<Attribute>();
            for (const attribute of response) {
              attributes.push(new Attribute(attribute));
            }
            this.heroService.setHeroAttributes(attributes);
          });
          return;
        }
        this.curAttributes = hero.attributes;
        this.curAttributes$.next(hero.attributes);
      }
    });
    this.raceService.getCurRace$().subscribe(race => {
      if (race) {
        for (const attribute of race.attributes) {
          this.raceConstraints.push({
            name: attribute.name,
            min: attribute.min,
            max: attribute.max,
          });
        }
      }
    });
  }

  private loadAttributes(): Observable<IAttribute[]> {
    return this.http.get(this.attributesJSON) as Observable<IAttribute[]>;
  }

  public getCurAttributes$() {
    return this.curAttributes$;
  }

  public setAttributeValue(short: string, value: number) {
    this.curAttributes[this.indexOfAttribute(short)].value = value;
    this.update();
  }

  public incrementAttributeValue(short: string) {
    ++this.curAttributes[this.indexOfAttribute(short)].value;
    this.update();
  }

  public decrementAttributeValue(short: string) {
    --this.curAttributes[this.indexOfAttribute(short)].value;
    this.update();
  }

  private update() {
    for (const attribute of this.curAttributes) {
      const constraint = this.raceConstraints.find(
        item => item.name === attribute.name
      );
      if (constraint) {
        if (attribute.value < constraint.min) {
          this.setAttributeValue(attribute.short, constraint.min);
          return;
        }
        if (attribute.value > constraint.max) {
          this.setAttributeValue(attribute.short, constraint.max);
          return;
        }
      }
    }
    this.heroService.setHeroAttributes(this.curAttributes);
  }

  private indexOfAttribute(short: string) {
    return this.curAttributes.findIndex(item => item.short === short);
  }
}
