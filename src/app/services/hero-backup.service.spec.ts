import { TestBed, inject } from '@angular/core/testing';

import { HeroBackupService } from './hero-backup.service';

describe('HeroBackupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeroBackupService]
    });
  });

  it('should be created', inject([HeroBackupService], (service: HeroBackupService) => {
    expect(service).toBeTruthy();
  }));
});
