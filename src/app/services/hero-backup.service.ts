// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Injectable } from '@angular/core';

import { Hero } from './../classes/hero';

@Injectable()
export class HeroBackupService {
  private appKey = 'ACreator-Backup';
  private backups: IBackup[];
  constructor() {
    this.backups = this.getLocalStorageData() ? this.getLocalStorageData() : [];
  }

  // Legt ein Backup eines Heldenobjekts an
  public backup(hero: Hero) {
    const curDate = new Date();
    // Prüft, ob das abzulegende Heldenobjekt mehr als zehn Sekunden jünger als das bisher aktuellste Objekt ist.
    if (new Date(+this.findLatestBackup(hero.name).date + 10000) >= curDate) {
      return;
    }
    const index = this.indexOfHero(hero.name);
    if (typeof index !== 'undefined') {
      this.backups[index].backups.push({
        hero,
        date: curDate,
      });
    }
    this.update();
  }

  private prune() {
    for (const item of this.backups) {
      if (item.backups.length >= 30) {
        item.backups.splice(
          item.backups.indexOf(this.findOldestBackup(item.name)),
          1
        );
      }
    }
  }

  private update() {
    this.prune();
    localStorage.setItem(this.appKey, JSON.stringify(this.backups));
  }

  private findLatestBackup(name: string): IBackupItem {
    const index = this.indexOfHero(name);
    if (typeof index === 'undefined') {
      return { hero: new Hero(''), date: new Date(0) };
    }
    let latest: IBackupItem;
    for (const item of this.backups[index].backups) {
      latest = item.date >= latest.date ? item : latest;
    }
    return latest;
  }

  private findOldestBackup(name: string) {
    const index = this.indexOfHero(name);
    let oldest: IBackupItem;
    for (const item of this.backups[index].backups) {
      oldest = item.date <= oldest.date ? item : oldest;
    }
    return oldest;
  }

  private getLocalStorageData(): IBackup[] {
    try {
      return JSON.parse(localStorage.getItem(this.appKey)) as IBackup[];
    } catch (e) {
      this.resetData();
      return this.getLocalStorageData();
    }
  }

  private resetData(): any {
    this.backups = [];
    localStorage.clear();
  }

  private indexOfHero(heroName: string): number {
    for (const item of this.backups) {
      if (item.name === heroName) {
        return this.backups.indexOf(item);
      }
    }
    return undefined;
  }
}

interface IBackup {
  name: string;
  backups: IBackupItem[];
}
interface IBackupItem {
  hero: Hero;
  date: Date;
}
