// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { HeroBackupService } from './hero-backup.service';

import { Attribute } from '../classes/attributes';
import { Hero } from './../classes/hero';
import { IRace } from './../classes/race';
import { ISkill } from './../classes/skills';

@Injectable()
export class HeroService {
  private appKey = 'ACreator';
  private hero: Hero;
  private heroes: Hero[];
  private currentHero$ = new BehaviorSubject<Hero>(undefined);
  private heroList$ = new BehaviorSubject<string[]>(undefined);

  constructor(private backupServ: HeroBackupService) {
    this.heroes = this.getLocalStorageData()
      ? this.getLocalStorageData()
      : new Array<Hero>();
    for (const hero of this.heroes) {
      this.heroes.splice(
        this.indexOfHero(hero.name),
        1,
        new Hero(hero.name, hero)
      );
    }
    this.update();
  }

  public getHeroList$(): BehaviorSubject<string[]> {
    return this.heroList$;
  }

  public updateHeroList() {
    const heroList = new Array<string>();
    for (const hero of this.heroes) {
      if (hero) {
        heroList.push(hero.name);
      }
    }
    this.heroList$.next(heroList);
  }

  public getCurrentHero$(): BehaviorSubject<Hero> {
    return this.currentHero$;
  }

  public setCurrentHero(name?: string) {
    if (typeof name !== 'undefined') {
      this.hero = this.getHero(name);
    } else {
      this.hero = undefined;
    }
    this.currentHero$.next(this.hero);
  }

  public heroExists(name: string): boolean {
    if (this.getHero(name)) {
      return true;
    } else {
      return false;
    }
  }

  public createNewHero(name?: string): Hero {
    if (!name) {
      name = 'Neuer Held';
    }
    const newHero = new Hero(this.getUnusedName(name));
    this.hero = newHero;
    this.update();
    this.setCurrentHero(newHero.name);
    return newHero;
  }

  public removeHero(name: string) {
    if (this.heroExists(name)) {
      this.heroes.splice(this.indexOfHero(name), 1);
      if (this.hero.name === name) {
        this.hero = undefined;
      }
      this.update();
    }
  }

  public setHeroRace(race: IRace) {
    this.hero.race = race;
    this.update();
  }

  public setHeroName(oldName: string, newName: string) {
    if (newName === null) {
      console.log('Kann den Namen nicht zu nichts ändern');
      return;
    }
    this.hero.name = this.getUnusedName(newName);
    // removeHero löscht den alten Eintrag aus dem Speicher
    this.removeHero(oldName);
  }

  public setHeroAttributes(attributes: Attribute[]) {
    this.hero.attributes = attributes;
    this.update();
  }

  public getHeroAttributes(): Attribute[] {
    return this.hero.attributes;
  }

  public setHeroSkills(skills: ISkill[]) {
    this.hero.skills = skills;
    this.update();
  }

  public getHeroSkills(): ISkill[] {
    return this.hero.skills;
  }

  private getHero(heroName: string): Hero {
    if (this.heroes.length === 0) {
      return undefined;
    }
    return this.heroes.find(hero => hero.name === heroName);
  }

  private indexOfHero(heroName: string): number {
    for (const item of this.heroes) {
      if (item.name === heroName) {
        return this.heroes.indexOf(this.getHero(heroName));
      }
    }
    return undefined;
  }

  private getLocalStorageData(): Hero[] {
    try {
      return JSON.parse(localStorage.getItem(this.appKey)) as Hero[];
    } catch (e) {
      this.resetData();
      return this.getLocalStorageData();
    }
  }

  private update() {
    if (this.heroes[0] === null) {
      this.heroes = new Array<Hero>();
    }
    if (this.hero) {
      if (this.heroExists(this.hero.name)) {
        this.heroes.splice(this.indexOfHero(this.hero.name), 1, this.hero);
      } else {
        this.heroes.push(this.hero);
      }
      this.backupServ.backup(this.hero);
      this.currentHero$.next(this.hero);
    }
    localStorage.setItem(this.appKey, JSON.stringify(this.heroes));
    this.updateHeroList();
  }

  private resetData(): any {
    this.heroes = new Array<Hero>();
    localStorage.clear();
    this.update();
  }

  private getUnusedName(desiredName: string): string {
    if (this.getHero(desiredName)) {
      for (let i = 1; ; i++) {
        if (!this.getHero(desiredName + '-' + i)) {
          return desiredName + '-' + i;
        }
      }
    }
    return desiredName;
  }
}
