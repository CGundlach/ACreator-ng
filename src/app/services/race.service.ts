// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { HeroService } from './hero.service';

import { BehaviorSubject } from 'rxjs';

import { IRace } from './../classes/race';

@Injectable()
export class RaceService {
  private raceList$ = new BehaviorSubject<string[]>(new Array<string>());
  private curRace$ = new BehaviorSubject<IRace>(undefined);
  private races = new Array<IRace>();
  private initialized$ = new BehaviorSubject<boolean>(false);
  private racesJSON = '../../assets/data/races.json';
  private curHero$ = this.heroService.getCurrentHero$();

  constructor(private http: HttpClient, private heroService: HeroService) {
    this.initialized$.next(false);
    this.loadRaces();
    this.curHero$.subscribe(hero => {
      if (typeof hero !== 'undefined' && hero.race) {
        this.curRace$.next(hero.race);
      } else {
        this.curRace$.next(undefined);
      }
    });
  }

  public getRaceList$(): BehaviorSubject<string[]> {
    return this.raceList$;
  }

  public getCurRace$(): BehaviorSubject<IRace> {
    return this.curRace$;
  }

  public setCurRace(raceName?: string) {
    raceName
      ? this.heroService.setHeroRace(
          this.races.find(race => race.name === raceName)
        )
      : this.heroService.setHeroRace(undefined);
  }

  private getRace(raceName: string): IRace {
    return this.races.find(race => race.name === raceName);
  }

  public checkInitialized$(): BehaviorSubject<boolean> {
    return this.initialized$;
  }

  private async loadRaces() {
    await this.http.get(this.racesJSON).subscribe(res => {
      const races = new Array<IRace>();
      const raceList = new Array<string>();
      const racesObject = res as IRace[];
      for (let race of racesObject) {
        race = this.updateMovementSpeed(race);
        raceList.push(race.name);
        races.push(race);
      }
      this.raceList$.next(raceList);
      this.races = races;
      this.initialized$.next(true);
    });
  }

  private updateMovementSpeed(race: IRace): IRace {
    if (!race.moveSpeed.march) {
      race.moveSpeed.march = race.moveSpeed.combat * 2;
    }
    if (!race.moveSpeed.sprint) {
      race.moveSpeed.sprint = race.moveSpeed.combat * 8;
    }
    if (!race.moveSpeed.swim) {
      race.moveSpeed.swim = race.moveSpeed.combat;
    }
    if (race.moveSpeedAir) {
      if (!race.moveSpeedAir.march) {
        race.moveSpeedAir.march = race.moveSpeedAir.combat * 2;
      }
      if (!race.moveSpeedAir.sprint) {
        race.moveSpeedAir.sprint = race.moveSpeedAir.combat * 8;
      }
      if (!race.moveSpeedAir.swim) {
        race.moveSpeedAir.swim = race.moveSpeedAir.combat;
      }
    }
    return race;
  }
}
