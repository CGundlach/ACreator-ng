// ACreator-ng
//
// Copyright 2018 Christopher Gundlach

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ISkill } from '../classes/skills';
import { HeroService } from './hero.service';

@Injectable({
  providedIn: 'root',
})
export class SkillsService {
  private skillsJSON = './../../assets/data/skills.json';
  private skillList: ISkill[] = [];
  private skillList$ = new BehaviorSubject<ISkill[]>([]);

  private loaded = false;

  private deferred = [];

  private curSkills$ = new BehaviorSubject<ISkill[]>([]);
  private curSkills: ISkill[] = [];

  private curHero$ = this.heroService.getCurrentHero$();

  // TODO: Rassen-/Originskills dürfen nicht unter den Startwert gesenkt, und nicht entfernt werden

  constructor(private heroService: HeroService, private http: HttpClient) {
    this.loadSkills();
    this.curHero$.subscribe(hero => {
      if (hero) {
        if (typeof hero.skills === 'undefined') {
          if (typeof hero.race !== 'undefined') {
            for (const skill of hero.race.skillBoni) {
              this.setSkill(
                skill.name,
                skill.bonus,
                skill.bonus + ' aus Rassenbonus'
              );
            }
          }
        } else {
          this.curSkills = hero.skills;
          this.curSkills$.next(hero.skills);
        }
      }
    });
  }

  public getSkillList$(): BehaviorSubject<ISkill[]> {
    return this.skillList$;
  }

  public getCurSkills$() {
    return this.curSkills$;
  }

  private update() {
    for (const skill of this.curSkills) {
      if (skill.rank > 10) {
        skill.rank = 10;
      } else if (skill.rank < 0) {
        skill.rank = 0;
      }
    }
    this.heroService.setHeroSkills(this.curSkills);
  }

  public addSkill(name: string) {
    if (this.skillExists(name)) {
      return;
    }
    this.setSkill(name, 0);
  }

  public setSkill(name: string, rank: number, notes?: string) {
    if (!this.loaded) {
      this.deferred.push({ name, rank, notes });
      return;
    }
    const skill = this.getSkillInfo(name);
    if (typeof skill === 'undefined') {
      console.log('Fehler: kein Skill mit dem Namen ' + name + ' gefunden');
      return;
    }
    skill.rank = rank;
    if (this.skillExists(name)) {
      this.curSkills.splice(this.skillIndex(name), 1, skill);
    } else {
      this.curSkills.push(skill);
    }
    this.update();
  }

  public removeSkill(name: string) {
    if (this.skillExists(name)) {
      this.curSkills.splice(this.skillIndex(name), 1);
    }
    this.update();
  }

  public incSkill(name: string) {
    if (this.skillExists(name)) {
      ++this.curSkills[this.skillIndex(name)].rank;
      this.update();
    }
  }

  public decSkill(name: string) {
    if (this.skillExists(name)) {
      --this.curSkills[this.skillIndex(name)].rank;
      this.update();
    }
  }

  public setNotes(name: string, notes: string) {
    if (this.skillExists(name)) {
      this.curSkills[this.skillIndex(name)].notes = notes;
      this.update();
    }
  }

  public reorder(name: string, newPosition: number) {
    if (this.skillExists(name) && this.curSkills.length >= newPosition) {
      const index = this.skillIndex(name);
      this.curSkills.splice(newPosition, 0, this.curSkills.splice(index, 1)[0]);
    }
    this.update();
  }

  private getSkillInfo(name: string): ISkill {
    return this.skillList.find(skill => skill.name === name);
  }

  private loadSkills() {
    this.http.get(this.skillsJSON).subscribe(
      res => {
        this.skillList = res as ISkill[];
        this.skillList$.next(this.skillList);
        this.loaded = true;
      },
      error => console.log('Fehler', error),
      () => {
        for (const skill of this.deferred) {
          this.setSkill(skill.name, skill.rank, skill.notes);
        }
        this.deferred = [];
      }
    );
  }

  private skillIndex(name: string) {
    return this.curSkills.findIndex(skill => skill.name === name);
  }

  private skillExists(name: string): boolean {
    return this.skillIndex(name) >= 0 ? true : false;
  }
}
